//mockdata for courses
let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Incididunt aliquip laboris nulla voluptate pariatur veniam commodo magna reprehenderit consequat do.",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Incididunt aliquip laboris nulla voluptate pariatur veniam commodo magna reprehenderit consequat do.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Incididunt aliquip laboris nulla voluptate pariatur veniam commodo magna reprehenderit consequat do.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "ABCDE",
		description: "Incididunt aliquip laboris nulla voluptate pariatur veniam commodo magna reprehenderit consequat do.",
		price: 40500,
		onOffer: false
	}

];

export default coursesData;