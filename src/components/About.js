//import react-bootstrap components
import {Row, Col} from 'react-bootstrap'

export default function About () {

	return (

		<Row className="bg-success text-light">
			<Col className="p-5">
				<h1 className="my-5">About Me</h1>
				<h2 className="mt-3">Nikko Lagat</h2>
				<h3>Full Stack Web Developer</h3>
				<p className="mb-4">
					A developer developing a development in a developing area enveloped with antelopes. 
				</p>
				<h4>Contact Me</h4>
				<ul>
					<li>Email: gnikkolagat@gmail.com</li>
					<li>Mobile No: +63 975 096 4567</li>
					<li>Address: San Jose Del Monte City, Bulacan</li>
				</ul>
			</Col>
		</Row>

	)

}