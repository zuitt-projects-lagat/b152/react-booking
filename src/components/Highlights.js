import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights ({highlightsProp}){

	console.log(highlightsProp);//value=undefined

	return (

			<Row className="my-3">
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Learn From Home</h2>
							</Card.Title>
							<Card.Text>
								In sed duis veniam dolore incididunt cillum enim duis est ut minim aute deserunt esse minim.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum sed ad nostrud ut tempor aute in velit proident ex in eiusmod ex veniam sunt sunt qui anim qui mollit non anim ad deserunt irure eiusmod voluptate in exercitation dolor amet esse exercitation.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Ex non dolor enim proident ullamco nostrud in culpa esse enim qui. Culpa minim ut adipisicing veniam enim amet voluptate et minim reprehenderit fugiat consequat non veniam nostrud sit culpa.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)

}